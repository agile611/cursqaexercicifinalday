package com.itnove.ba.wordpress.tests;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.wordpress.pages.WordPressLoginPage;
import org.junit.Test;

public class LoginCorrecteTestSauce extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://wordpress.votarem.lu/wp-login.php");
        WordPressLoginPage loginPage = new WordPressLoginPage(driver);
        loginPage.login("admin", "admin");
    }
}
