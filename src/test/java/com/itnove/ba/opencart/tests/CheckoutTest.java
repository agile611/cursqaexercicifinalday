package com.itnove.ba.opencart.tests;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pages.CheckoutPage;
import com.itnove.ba.opencart.pages.DashboardPage;
import com.itnove.ba.opencart.pages.SearchResultsPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CheckoutTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://opencart.votarem.lu");
        Thread.sleep(3000);

        //Cerco l'article
        DashboardPage dashboardPage = new DashboardPage(driver);
        dashboardPage.searchArticle("iPhone");
        Thread.sleep(3000);
        //Comprobo que els resultats coincideixen amb el paràmetre de cerca
        assertEquals("iPhone", "iPhone");
        //Afegeixo al carret de la compra
        SearchResultsPage addToCartButton = new SearchResultsPage(driver);
        addToCartButton.clickOnAddToCartButton();
        Thread.sleep(3000);
        //Vaig a fer el checkout
        DashboardPage checkout = new DashboardPage(driver);
        checkout.checkOutButton.click();
        Thread.sleep(3000);
        DashboardPage petitButton = new DashboardPage(driver);
        petitButton.tinyButton.click();
        Thread.sleep(3000);
        CheckoutPage button = new CheckoutPage(driver);
        button.isCheckoutButtonPresent();
    }
}
