package com.itnove.ba.opencart.tests;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pages.LoginPage;
import org.testng.annotations.Test;
import com.itnove.ba.opencart.pages.ForgottenPasswordPage;

import static org.testng.Assert.assertTrue;


public class ForgottenPasswordTestWithWrongEmailTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.get("http://opencart.votarem.lu/admin/index.php");
       LoginPage loginPage = new LoginPage(driver);
       loginPage.forgottenPassword.click();
       Thread.sleep(3000);

       // Accessed password reminder page
       ForgottenPasswordPage forgottenPassPage = new ForgottenPasswordPage(driver);
       forgottenPassPage.fillAndSendWrongEmail();
       assertTrue(forgottenPassPage.isErrorMessagePresent(driver, wait));
    }
}
