package com.itnove.ba.opencart.tests;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.pages.DashboardPage;
import com.itnove.ba.opencart.pages.LoginPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LoginCorrecteTestSauce extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        System.out.println(driver.getSessionId());
        driver.get("http://opencart.votarem.lu/admin/");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami1");
        DashboardPage dashboardPage = new DashboardPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
    }
}
