package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage {

    private WebDriver driver;

    @FindBy(xpath = "id(\"content\")/div[1]/div[1]/h1[1]")
    public WebElement Dashboard;

    @FindBy(xpath = "id(\"top-links\")/ul[1]/li[5]/a[1]/span[1]")
    public WebElement checkOutButton;

    @FindBy(xpath = "id(\"search\")/span[1]/button[1]")
    public WebElement searchButton;

    @FindBy(xpath = "id(\"search\")/input[1]")
    public WebElement searchBox;

    @FindBy(xpath = "id(\"collapse-checkout-option\")/div[1]/div[1]/div[1]/div[2]/label[1]/input[1]")
    public WebElement tinyButton;

    public void searchArticle(String keyword) {
        searchBox.click();
        searchBox.sendKeys(keyword);
        searchButton.click();
    }

    public boolean isDashboardLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(Dashboard));
        return Dashboard.isDisplayed() &&
               Dashboard.getText().equals("Dashboard");
    }


    public DashboardPage(WebDriver driver) {

        PageFactory.initElements(driver, this);
    }

}
