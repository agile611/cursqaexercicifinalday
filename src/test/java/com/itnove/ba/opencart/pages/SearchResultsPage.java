package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultsPage {

    private WebDriver driver;

    @FindBy(xpath = "id(\"content\")/div[3]/div[1]/div[1]/div[2]/div[2]/button[1]")
    public WebElement addToCartButton;

//    @FindBy(xpath = ".//*[@class='search_form']")
//    public WebElement searchForm;
//
//    @FindBy(id = "searchFieldMain")
//    public WebElement searchFieldMain;

    @FindBy(xpath = "id(\"content\")/div[3]/div[1]/div[1]/div[2]/div[2]/button[1]")
    public WebElement results;
//
//    @FindBy(xpath = ".//*[@id='pagecontent']/table/tbody[2]/tr/td")
//    public WebElement noResults;

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }
//
//    public boolean isSearchResultsPageLoaded(WebDriverWait wait){
//        wait.until(ExpectedConditions.visibilityOf(searchForm));
//        return searchForm.isDisplayed();
//    }
//
//    public boolean isNoSearchResultsDisplayed(WebDriverWait wait){
//        wait.until(ExpectedConditions.visibilityOf(noResults));
//        return noResults.isDisplayed();
//    }
//
//    public String isSearchKeywordCorrect(){
//        return searchFieldMain.getAttribute("value");
//    }
//    public void clickOnFirstResult(WebDriverWait wait) throws InterruptedException {
//        results.click();
//    }


    public SearchResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
