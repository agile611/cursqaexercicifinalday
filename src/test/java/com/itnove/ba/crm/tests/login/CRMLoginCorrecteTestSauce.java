package com.itnove.ba.crm.tests.login;
import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class CRMLoginCorrecteTestSauce extends BaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        System.out.println(driver.getSessionId());
        driver.navigate().to("http://crm.votarem.lu");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user", "bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        assertTrue(dashboardPage.isDashboardLoaded(driver, wait));

    }
}